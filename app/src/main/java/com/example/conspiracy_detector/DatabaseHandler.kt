package com.example.conspiracy_detector

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.jjoe64.graphview.series.DataPoint

class DatabaseHandler(context: Context) {
    private var helper: MyDbHelper

    init {
        helper = MyDbHelper(context)
    }

    fun insertData(tabName: String?, x0: Double?, y0: Double?, x1: Double?, y1: Double?): Long {

        val db = helper.writableDatabase
        //db.execSQL("Drop table if exists myTable;")
        db.execSQL(
            "CREATE TABLE IF NOT EXISTS " + tabName +
                    " ( _id INTEGER PRIMARY KEY AUTOINCREMENT, valX REAL, " +
                    "valY REAL, newX REAL, newY REAL);"
        )
        val contentValues = ContentValues().apply {
            put(MyDbHelper.valX, x0)
            put(MyDbHelper.valY, y1)
            put(MyDbHelper.valNewX, x1)
            put(MyDbHelper.valNewY, y0)
        }
        // return a primary key of new inserted row
        return db.insert(tabName, null, contentValues)
    }

    fun getAllTable(): ArrayList<String> {
        val db = helper.readableDatabase
        val listOfTableNames = ArrayList<String>()
        val c: Cursor = db.rawQuery("SELECT name FROM sqlite_sequence", null)

        if (c.moveToFirst()) {
            while (!c.isAfterLast) {
                listOfTableNames.add(c.getString(c.getColumnIndex("name")))
                c.moveToNext()
            }
        }
        c.close()
        return listOfTableNames
    }

    fun readFromTable(tabName: String?): ArrayList<DataPoint> {
        val db = helper.readableDatabase
        val listOfPoints = ArrayList<DataPoint>()
        val doubleArray: DoubleArray = doubleArrayOf(0.0, 0.0, 0.0, 0.0)
        val c: Cursor = db.rawQuery("SELECT * FROM $tabName", null)

        if (c.moveToFirst()) {
            while (!c.isAfterLast) {
                doubleArray[0] = c.getDouble(c.getColumnIndex(MyDbHelper.valX))
                doubleArray[1] = c.getDouble(c.getColumnIndex(MyDbHelper.valNewY))
                doubleArray[2] = c.getDouble(c.getColumnIndex(MyDbHelper.valNewX))
                doubleArray[3] = c.getDouble(c.getColumnIndex(MyDbHelper.valY))

                listOfPoints.add(DataPoint(doubleArray[0], doubleArray[1]))
                listOfPoints.add(DataPoint(doubleArray[2], doubleArray[3]))
                c.moveToNext()
            }
        }
        c.close()
        return listOfPoints
    }


    class MyDbHelper(context: Context) :
        SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_Version) {
        companion object {
            private var DATABASE_NAME: String = "myDatabase"
            private var DATABASE_Version = 1
            var valX: String = "valX"
            var valY: String = "valY"
            var valNewX: String = "newX"
            var valNewY: String = "newY"
        }

        override fun onCreate(db: SQLiteDatabase) {
            Log.i("Database created", "successfully")
        }

        override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
            try {
                Log.i("Not Implemented ", "yet")
            } catch (ex: Exception) {
            }
        }

    }

}
