package com.example.conspiracy_detector

import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.settings_activity.*
import java.util.*

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //read language
        val lang = getLagPref()
        setAppLocale(lang)
        setContentView(R.layout.settings_activity)

        //set action bar
        val actbar = supportActionBar
        actbar?.title = getString(R.string.settigs_title)


        val shpref = getSharedPreferences("conspiracySharedPef", MODE_PRIVATE)

        SaveSettings?.setOnClickListener {
            saveData(shpref)
        }


        //Toast.makeText(this,lang, Toast.LENGTH_LONG).show()

    }

    private fun saveData(shpref: SharedPreferences) {

        // Check which radio button is chosen
        val editShare = shpref.edit()
        when (radioGroup2.checkedRadioButtonId) {
            R.id.lanBtnPL -> {
                editShare.putString("lang", "pl")
                editShare.apply()

            }
            R.id.lanBtnEN -> {
                editShare.putString("lang", "en")
                editShare.commit()
            }
        }
        Toast.makeText(this, getString(R.string.restart_asked), Toast.LENGTH_LONG).show()

    }


    private fun setAppLocale(localeCode: String?) {
        if (localeCode != null) {

            val res = resources
            val dm = res.displayMetrics
            val con = res.configuration
            val loc = Locale(localeCode)

            con?.setLocale(loc)
            res.updateConfiguration(con, dm)
        }
    }

    private fun getLagPref(): String? {

        val sh = getSharedPreferences("conspiracySharedPef", 0)

        return sh.getString("lang", "en")
    }
}