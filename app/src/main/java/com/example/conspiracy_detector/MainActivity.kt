package com.example.conspiracy_detector


import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity(), SensorEventListener {

    //Globals for sensor
    private var mSensorManager: SensorManager? = null

    //Accelerometer [3]
    private var mySensorAccelerator: Sensor? = null
    private var mSensorLinAccel: Sensor? = null

    //Gyroscope[3]
    private var mySensorGyroscope: Sensor? = null

    //GRavity[3]
    private var mySensorGravity: Sensor? = null

    //Rotation[4]
    private var mySensorRotation: Sensor? = null

    //Magetic field[3]
    private var mySensorMag: Sensor? = null

    //Orientation[
    private var mySensorOrientation: Sensor? = null

    //Proximity[1]
    private var mySensorProximity: Sensor? = null

    //Temperature[1]
    private var mySensorAmbientTemperature: Sensor? = null

    //Light[1]
    private var mySensorLight: Sensor? = null

    //Globals for choosing sensor
    private var numParam: Int = 1
    private var sensorName: Int = 1


    //App life
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //read language
        val lang = getLagPref()
        setAppLocale(lang)
        setContentView(R.layout.activity_main)

        //set action bar
        val actbar = supportActionBar
        actbar?.title = getString(R.string.select_sensor_title)

        //Start sensor scan
        setSensors()
        buttonGo.text = getString(R.string.StartSpy)


        //Select sensor onClick

        buttonGo?.setOnClickListener {
            val intent = Intent(this, MainActivity2::class.java)
            // Start new activity with number of the values we want from sensor
            intent.putExtra("numParam", numParam)
            intent.putExtra("sensorName", sensorName)
            startActivity(intent)
        }

        linAccBtn?.setOnClickListener {
            sensorName = Sensor.TYPE_LINEAR_ACCELERATION
            numParam = 3
            BtnUpdate()
            linAccBtn.setBackgroundColor(Color.GREEN)
            info_Sensor_TextView?.text =
                getText(R.string.linear_acc_info)
        }

        normalAccBtn?.setOnClickListener {
            sensorName = Sensor.TYPE_ACCELEROMETER
            numParam = 3
            BtnUpdate()
            normalAccBtn.setBackgroundColor(Color.GREEN)
            info_Sensor_TextView?.text =
                getText(R.string.normal_acc_info)
        }

        gyroBtn?.setOnClickListener {
            sensorName = Sensor.TYPE_GYROSCOPE
            numParam = 3
            BtnUpdate()
            gyroBtn.setBackgroundColor(Color.GREEN)
            info_Sensor_TextView?.text =
                getText(R.string.Gyroscope_info)
        }

        GravBtn?.setOnClickListener {
            sensorName = Sensor.TYPE_GRAVITY
            numParam = 3
            BtnUpdate()
            GravBtn.setBackgroundColor(Color.GREEN)
            info_Sensor_TextView?.text =
                getText(R.string.gravity_info)
        }

        RotBtn?.setOnClickListener {
            sensorName = Sensor.TYPE_ROTATION_VECTOR
            numParam = 4
            BtnUpdate()
            RotBtn.setBackgroundColor(Color.GREEN)
            info_Sensor_TextView?.text =
                getText(R.string.rotation_info)
        }

        MagBtn?.setOnClickListener {
            sensorName = Sensor.TYPE_MAGNETIC_FIELD
            numParam = 3
            BtnUpdate()
            MagBtn.setBackgroundColor(Color.GREEN)
            info_Sensor_TextView?.text = getText(R.string.mag_info)
        }

        OrientBtn?.setOnClickListener {
            sensorName = Sensor.TYPE_ORIENTATION
            numParam = 3
            BtnUpdate()
            OrientBtn.setBackgroundColor(Color.GREEN)
            info_Sensor_TextView?.text =
                getText(R.string.orient_info)
        }

        ProxBtn?.setOnClickListener {
            sensorName = Sensor.TYPE_PROXIMITY
            numParam = 1
            BtnUpdate()
            ProxBtn.setBackgroundColor(Color.GREEN)
            info_Sensor_TextView?.text =
                getText(R.string.prox_info)
        }

        TempBtn?.setOnClickListener {
            sensorName = Sensor.TYPE_AMBIENT_TEMPERATURE
            numParam = 1
            BtnUpdate()
            TempBtn.setBackgroundColor(Color.GREEN)
            info_Sensor_TextView?.text =
                getText(R.string.temp_info)
        }

        LightBtn?.setOnClickListener {
            sensorName = Sensor.TYPE_LIGHT
            numParam = 1
            BtnUpdate()
            LightBtn.setBackgroundColor(Color.GREEN)
            info_Sensor_TextView?.text =
                getText(R.string.light_info)
        }
    }

    override fun onStart() {
        super.onStart()
        BtnUpdate()
    }

    override fun onStop() {
        super.onStop()
        mSensorManager?.unregisterListener(this)
    }

    //Listeners and onEvents
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.savesBtn -> {
                val intent = Intent(this, SavesActivity::class.java)
                // Start new activity with number of the values we want from sensor
                startActivity(intent)
            }
            R.id.settigs -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
            }
            R.id.info -> {
                val appName = this.getApplicationInfo()
                Toast.makeText(this, appName.toString(), Toast.LENGTH_SHORT).show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSensorChanged(event: SensorEvent?) {}

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        Log.i("build", "not today")
    }


    /// functions
    @SuppressLint("SetTextI18n")
    private fun setSensors() {
        mSensorManager = getSystemService(SENSOR_SERVICE) as SensorManager?
        var button = findViewById<Button>(R.id.normalAccBtn)


        // notka dla misi: !! == Add non null asserted call
        mySensorAccelerator = mSensorManager!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        if (mySensorAccelerator != null) {
            button.text = getText(R.string.normalAccBtn)
        } else {
            button.text = "Normal Accelerometer sensor " + getText(R.string.No_sensor)
            button.isEnabled = false
        }


        button = findViewById<Button>(R.id.linAccBtn)
        mSensorLinAccel = mSensorManager!!.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION)
        if (mSensorLinAccel != null) {
            button.text = getText(R.string.linearAccBtn)
        } else {
            button.text = "Linear Accelerometer sensor " + getText(R.string.No_sensor)
            button.isEnabled = false
        }

        button = findViewById<Button>(R.id.gyroBtn)
        mySensorGyroscope = mSensorManager!!.getDefaultSensor(Sensor.TYPE_GYROSCOPE)
        if (mySensorGyroscope != null) {
            button.text = getText(R.string.GyroBtn)
        } else {
            button.text = getText(R.string.No_sensor)
            button.isEnabled = false
        }

        button = findViewById<Button>(R.id.GravBtn)
        mySensorGravity = mSensorManager!!.getDefaultSensor(Sensor.TYPE_GRAVITY)
        if (mySensorGravity != null) {
            button.text = getText(R.string.gravityBtn)
        } else {
            button.text = getText(R.string.No_sensor)
            button.isEnabled = false
        }

        button = findViewById<Button>(R.id.RotBtn)
        mySensorRotation = mSensorManager!!.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)
        if (mySensorRotation != null) {

            button.text = getText(R.string.RottBtn)
        } else {
            button.text = getText(R.string.No_sensor)
            button.isEnabled = false
        }

        button = findViewById<Button>(R.id.MagBtn)
        mySensorMag = mSensorManager!!.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)
        if (mySensorMag != null) {
            button.text = getText(R.string.gravityBtn)
        } else {
            button.text = getText(R.string.No_sensor)
            button.isEnabled = false
        }

        button = findViewById<Button>(R.id.OrientBtn)
        mySensorOrientation = mSensorManager!!.getDefaultSensor(Sensor.TYPE_ORIENTATION)
        if (mySensorOrientation != null) {
            button.text = getText(R.string.orientationBtt)
        } else {
            button.text = getText(R.string.No_sensor)
            button.isEnabled = false
        }

        button = findViewById<Button>(R.id.ProxBtn)
        mySensorProximity = mSensorManager!!.getDefaultSensor(Sensor.TYPE_PROXIMITY)
        if (mySensorProximity != null) {
            button.text = getText(R.string.ProxBtn)
        } else {
            button.text = getText(R.string.No_sensor)
            button.isEnabled = false
        }

        button = findViewById<Button>(R.id.TempBtn)
        mySensorAmbientTemperature =
            mSensorManager!!.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE)
        if (mySensorAmbientTemperature != null) {
            button.text = getText(R.string.TemBtn)
        } else {
            button.text = getText(R.string.No_sensor)
            button.isEnabled = false
        }


        button = findViewById<Button>(R.id.LightBtn)
        mySensorLight = mSensorManager!!.getDefaultSensor(Sensor.TYPE_LIGHT)
        if (mySensorLight != null) {
            button.text = getText(R.string.LightBtn)
        } else {
            button.text = getText(R.string.No_sensor)
            button.isEnabled = false
        }
    }

    private fun setAppLocale(localeCode: String?) {
        if (localeCode != null) {

            val res = resources
            val dm = res.displayMetrics
            val con = res.configuration
            val loc = Locale(localeCode)

            con?.setLocale(loc)
            res.updateConfiguration(con, dm)
        }
    }

    private fun getLagPref(): String? {
        val sh = getSharedPreferences("conspiracySharedPef", 0)
        return sh.getString("lang", "en")
    }

    private fun BtnUpdate() {
        LightBtn.setBackgroundColor(Color.LTGRAY)
        OrientBtn.setBackgroundColor(Color.LTGRAY)
        TempBtn.setBackgroundColor(Color.LTGRAY)
        GravBtn.setBackgroundColor(Color.LTGRAY)
        linAccBtn.setBackgroundColor(Color.LTGRAY)
        normalAccBtn.setBackgroundColor(Color.LTGRAY)
        ProxBtn.setBackgroundColor(Color.LTGRAY)
        MagBtn.setBackgroundColor(Color.LTGRAY)
        RotBtn.setBackgroundColor(Color.LTGRAY)
        gyroBtn.setBackgroundColor(Color.LTGRAY)
    }

}
