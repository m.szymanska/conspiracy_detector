package com.example.conspiracy_detector

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import kotlinx.android.synthetic.main.activity_main2.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class MainActivity2 : AppCompatActivity(), SensorEventListener {

    private var mSensorManager: SensorManager? = null
    private var myChosenSensor: Sensor? = null
    private var numParam: Int = 1
    private var sensorName: Int = 1
    private var nextXPosition: Double = 1.0
    private var lastPositions: MutableList<Double> = mutableListOf()
    private var myColors: MutableList<Int> = mutableListOf()
    private var onSensorChangeLock: Boolean = false
    private lateinit var dbHandler: DatabaseHandler
    private var scrollToEnd: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //read language
        val lang = getLagPref()
        setAppLocale(lang)
        setContentView(R.layout.activity_main2)

        //set action bar
        val actbar = supportActionBar
        actbar?.title = getString(R.string.sensor_read_title)

        // Get the number of values from intent that started the activity
        numParam = intent.extras?.getInt("numParam") ?: 1
        sensorName = intent.extras?.getInt("sensorName") ?: 1

        // Get handle to local database
        dbHandler = DatabaseHandler(this)

        mSensorManager = getSystemService(SENSOR_SERVICE) as SensorManager?
        myChosenSensor = mSensorManager!!.getDefaultSensor(sensorName)

        //val button = findViewById<Button>(R.id.buttonMain2Back) // is it necessary?
        buttonMain2Back?.setOnClickListener {
            // close the activity and return to previous one
            finish()
        }
        buttonSave?.setOnClickListener {
            // Save in local db, if ok show Toast
            Toast.makeText(this, getString(R.string.saving_info), Toast.LENGTH_SHORT).show()
            addGraphToDb()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.savesBtn -> {
                val intent = Intent(this, SavesActivity::class.java)
                // Start new activity with number of the values we want from sensor
                startActivity(intent)
            }
            R.id.settigs -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
            }
            R.id.info -> {
                val appName = this.getApplicationInfo()
                Toast.makeText(this, appName.toString(), Toast.LENGTH_SHORT).show()
            }
        }
        return super.onOptionsItemSelected(item)
    }


    @SuppressLint("ClickableViewAccessibility", "SetTextI18n")
    override fun onSensorChanged(event: SensorEvent?) {
        if (onSensorChangeLock) {
            Log.i("Event sensor lock", "Too fast")
            return
        }
        onSensorChangeLock = true

        Thread {
            Thread.sleep(500)
            this@MainActivity2.runOnUiThread {
                when (event!!.sensor.type) {
                    sensorName -> {
                            main2X?.text = "1st value is: " + String.format("%.2f", event.values[0])
                            main2Y?.text = "2nd value is: " + String.format("%.2f", event.values[1])
                            main2Z?.text = "3rd value is: " + String.format("%.2f", event.values[2])

                        // just in landscape orientation
                        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                            val graph = findViewById<GraphView>(R.id.graphViewXML)

                            for (i in 0 until numParam) {
                                val lastPoint = DataPoint(nextXPosition - 1, lastPositions[i])
                                val series = LineGraphSeries(
                                    arrayOf<DataPoint>(
                                        lastPoint,
                                        DataPoint(nextXPosition, (event.values[i]).toDouble())
                                    )
                                )
                                series.color = myColors[i]
                                series.thickness = 9
                                graph.addSeries(series)
                                lastPositions[i] = event.values[i].toDouble()
                            }
                            nextXPosition += 1
                            if (scrollToEnd) {
                                graphViewXML?.viewport?.scrollToEnd()
                            }
                            onSensorChangeLock = false

                            if (!graphViewXML.viewport.isScalable) {
                                graphViewXML?.viewport?.setScalableY(true)
                                graphViewXML?.title = getString(R.string.graph_title)
                                graphViewXML?.titleTextSize = 40f
                                graphViewXML?.viewport?.setMinX(0.0)
                                graphViewXML?.viewport?.setMaxX(10.0)

                                graphViewXML?.setOnTouchListener { _, event ->
                                    when (event.action) {
                                        MotionEvent.ACTION_DOWN -> {
                                            scrollToEnd = false
                                        }
                                        MotionEvent.ACTION_UP -> {
                                            scrollToEnd = true
                                        }
                                    }
                                    false
                                }
                            }
                        }
                    }
                    else -> {
                        // do nothing
                        Toast.makeText(
                            this, "Seems that sensor " +
                                    "is not working today", Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }
        }.start()
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        Log.i("build", "not today")
    }

    override fun onStart() {
        super.onStart()
        if (myChosenSensor != null) {
            mSensorManager?.registerListener(
                this,
                myChosenSensor,
                1_000_000,  // seems that its not working at all
                1_000_000
            )
        }
        for (i in 0 until numParam) {
            lastPositions.add(0.0)
        }
        myColors.add(Color.RED)
        myColors.add(Color.BLUE)
        myColors.add(Color.GREEN)
    }

    override fun onStop() {
        super.onStop()
        mSensorManager?.unregisterListener(this)
    }

    private fun addGraphToDb() {
        //get the current date and time to table name
        val currentDateTime = LocalDateTime.now()
        val time = currentDateTime.format(DateTimeFormatter.ofPattern("yy_MM_dd__HH_mm"))
        var tabName = (myChosenSensor?.name + time).replace("\\s".toRegex(), "")
        tabName = tabName.replace("-", "_")

        graphViewXML?.series?.forEach loop@{
            if (it.lowestValueX == it.highestValueX) {
                return@loop
            }
            val points = it.getValues(it.lowestValueX, it.highestValueX)

            val tempPoints: MutableList<DataPoint> = mutableListOf()
            var counter = 0
            points.forEach {
                tempPoints.add(counter, it as DataPoint)
                counter += 1
            }

            val result = dbHandler.insertData(
                tabName,
                tempPoints[0].x, tempPoints[0].y, tempPoints[1].x, tempPoints[1].y
            )
            if (result < 0) {
                Toast.makeText(
                    this,
                    getString(R.string.database_save_unsuccesful),
                    Toast.LENGTH_SHORT
                ).show()
                return
            }
        }
        Toast.makeText(this, getString(R.string.database_save_succesful), Toast.LENGTH_SHORT).show()
    }

    private fun setAppLocale(localeCode: String?) {
        if (localeCode != null) {
            val res = resources
            val dm = res.displayMetrics
            val con = res.configuration
            val loc = java.util.Locale(localeCode)
            con?.setLocale(loc)
            res.updateConfiguration(con, dm)
        }
    }

    private fun getLagPref(): String? {
        val sh = getSharedPreferences("conspiracySharedPef", 0)
        return sh.getString("lang", "en")
    }

}
