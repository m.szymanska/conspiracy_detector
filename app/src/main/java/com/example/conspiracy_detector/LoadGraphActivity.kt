package com.example.conspiracy_detector

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import kotlinx.android.synthetic.main.activity_main2.*
import java.util.*

class LoadGraphActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //read language
        val lang = getLagPref()
        setAppLocale(lang)
        setContentView(R.layout.activity_load_graph)

        //set action bar
        val actbar = supportActionBar


        // Get handle to local database
        val dbHandler = DatabaseHandler(this)

        // Get the name of chosen table
        val chosenTable = intent.extras?.getString("chosenTable") ?: "myTable"

        actbar?.title = chosenTable
        makeGraph(dbHandler, chosenTable)
    }

    private fun makeGraph(dbHandler: DatabaseHandler, tableName: String) {

        val allDataPoints = dbHandler.readFromTable(tableName)
        var counter = 0

        val graph = findViewById<GraphView>(R.id.loadedGraphViewXML)
        while (counter < allDataPoints.size) {
            //for ( i in 0 until allDataPoints.size/2) {
            val series = LineGraphSeries(arrayOf<DataPoint>())
            series.appendData(allDataPoints[counter++], true, allDataPoints.size)
            series.appendData(allDataPoints[counter++], true, allDataPoints.size)
            graph.addSeries(series)
        }
        if (!graph.viewport.isScalable) {
            setScrollable()
        }

    }

    private fun setScrollable() {
        val graph = findViewById<GraphView>(R.id.loadedGraphViewXML)
        graph?.viewport?.setScalableY(true)
        graph?.title = getString(R.string.graph_title)
        graph?.titleTextSize = 40f
        graph?.viewport?.setMinX(0.0)
        graph?.viewport?.setMaxX(10.0)
    }

    private fun setAppLocale(localeCode: String?) {
        if (localeCode != null) {

            val res = resources
            val dm = res.displayMetrics
            val con = res.configuration
            val loc = Locale(localeCode)

            con?.setLocale(loc)
            res.updateConfiguration(con, dm)
        }
    }

    private fun getLagPref(): String? {

        val sh = getSharedPreferences("conspiracySharedPef", 0)

        return sh.getString("lang", "en")
    }

}
