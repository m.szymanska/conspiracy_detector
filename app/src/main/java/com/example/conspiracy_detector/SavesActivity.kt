package com.example.conspiracy_detector

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_saves.*
import java.util.*
import kotlin.collections.ArrayList

class SavesActivity : AppCompatActivity(), AdapterList.OnItemClickListener {

    private lateinit var dbHandler: DatabaseHandler
    private lateinit var listOfSaves: ArrayList<Item>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //read language
        val lang = getLagPref()
        setAppLocale(lang)
        setContentView(R.layout.activity_saves)

        //set action bar
        val actbar = supportActionBar
        actbar?.title = getString(R.string.saves_title)

        // Get handle to local database
        dbHandler = DatabaseHandler(this)

        listOfSaves = createList(dbHandler)
        recycler_view.adapter = AdapterList(listOfSaves, this)
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.setHasFixedSize(true)
    }

    override fun onItemClick(position: Int) {
        val item = listOfSaves[position]
        val text = item.text1

        val intent = Intent(this, LoadGraphActivity::class.java)
        // Start new activity with chosen element to create graph from
        intent.putExtra("chosenTable", text)
        startActivity(intent)
    }

    private fun createList(dbHandler: DatabaseHandler): ArrayList<Item> {

        val allTableName = dbHandler.getAllTable()
        val list = ArrayList<Item>()

        for (i in 0 until allTableName.size) {
            val item = Item(allTableName[i])
            list += item
        }
        return list
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.savesBtn -> {
            }
            R.id.settigs -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
            }
            R.id.info -> {
                val appName = this.applicationInfo
                Toast.makeText(this, appName.toString(), Toast.LENGTH_SHORT).show()
            }
        }
        return super.onOptionsItemSelected(item)
    }


    private fun setAppLocale(localeCode: String?) {
        if (localeCode != null) {

            val res = resources
            val dm = res.displayMetrics
            val con = res.configuration
            val loc = Locale(localeCode)

            con?.setLocale(loc)
            res.updateConfiguration(con, dm)
        }
    }

    private fun getLagPref(): String? {

        val sh = getSharedPreferences("conspiracySharedPef", 0)

        return sh.getString("lang", "en")
    }
}

